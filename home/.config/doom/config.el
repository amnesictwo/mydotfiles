;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Eliseu Lucena Barros"
      user-mail-address "eliseu42lucena@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)

;;; Load Odin Mode
(add-to-list 'load-path "/home/synbian/git/clone/Odin/odin-mode")
(require 'odin-mode)

;;; Load Jai Mode
(add-to-list 'load-path "/home/synbian/.config/doom/syntax-modes/jai-mode")
(require 'jai-mode)

;;; Load K Language Mode
(add-to-list 'load-path "/home/synbian/.config/doom/syntax-modes/")
(require 'k-mode)

;;; Load turtle mode
;; (add-to-list 'load-path "/home/synbian/.config/doom/syntax-modes/ttl-mode")
(require 'ttl-mode)

(add-to-list 'custom-theme-load-path (expand-file-name "~/.config/doom/themes/"))
;; (require 'naysayer-theme)
(load-theme 'naysayer t t)
(when (display-graphic-p)
  (setq doom-theme 'naysayer)
  (enable-theme 'naysayer))

(use-package all-the-icons
  :if (display-graphic-p))

(setq doom-themes-treemacs-theme "naysayer")

;; use cache for glyphs and do not load performance of CPU
(setq inhibit-compacting-font-caches t)


;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
;; (setq display-line-numbers-type nil)

;; display fill columns
(setq display-fill-column-indicator t)

;; disable line numbers
(setq display-line-numbers nil)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;;; cursor blink super fast
;; (defvar blink-cursor-interval-visible .1)
;; (defvar blink-cursor-interval-invisible .1)

(setq blink-cursor-blinks 0)
(setq blink-cursor-mode t)
(setq visible-cursor nil)

(defadvice internal-show-cursor (before unsymmetric-blink-cursor-interval)
  (when blink-cursor-timer
    (setf (timer--repeat-delay blink-cursor-timer)
          (if (internal-show-cursor-p)
              blink-cursor-interval-visible
            blink-cursor-interval-invisible))))

;; source :: https://github.com/VernonGrant/discovering-emacs/blob/main/show-notes/4-using-whitespace-mode.md
;; source: https://tech.tonyballantyne.com/2023/01/23/whitespace-mode/
(require 'color)
(let* ((ws-lighten 20) ;; Amount in percentage to lighten up black.
       (ws-color (color-lighten-name "#000000" ws-lighten)))
  (custom-set-faces
   `(whitespace-newline                ((t (:foreground ,ws-color))))
   `(whitespace-missing-newline-at-eof ((t (:foreground ,ws-color))))
   `(whitespace-space                  ((t (:foreground ,ws-color))))
   `(whitespace-space-after-tab        ((t (:foreground ,ws-color))))
   `(whitespace-space-before-tab       ((t (:foreground ,ws-color))))
   `(whitespace-tab                    ((t (:foreground ,ws-color))))
   `(whitespace-trailing               ((t (:foreground ,ws-color))))))

(use-package! whitespace
  :config
  (setq
   whitespace-style
   '(face spaces empty tabs newline trailing space-mark tab-mark newline-mark)
   whitespace-display-mappings
   '(
     ;; space -> · else .
     (space-mark 32 [183] [46])
     ;; (newline-mark ?\n [172 ?\n] [36 ?\n])
     ;; carriage return (Windows) -> ¶ else #
     (newline-mark ?\r [182] [35])
     ;; tabs -> » else >
     (tab-mark ?\t [187 ?\t] [62 ?\t])
     ;; (space-mark   ?\     [?\u00B7]     [?.])
     ;; (space-mark   ?\xA0  [?\u00A4]     [?_])
     ;; (newline-mark ?\n    [182 ?\n])
     ;; (tab-mark     ?\t    [?\u00BB ?\t] [?\\ ?\t])))
     (global-whitespace-mode +1))))


   ;; (ad-activate 'internal-show-cursor)


;;; slime iniciative


   (defun slime-eval-buffer-without-comments ()
  (interactive)
  (let ((old-buffer (current-buffer)))
    (with-temp-buffer
      (insert-buffer-substring old-buffer)
      (goto-char 0)
      (flush-lines "^#")
      (flush-lines "^|")
      (flush-lines "^exec")
      (slime-eval-buffer))))

;; batery mode

;; (setq display-battery-mode t)

;; relative line numbers

(defun display-relative-mode ()
  (interactive)
  (setq display-line-numbers-type 'relative)
  (display-line-numbers-mode)
  (display-line-numbers-mode))


;;;; Transparency for emacs
(defun transparency/emacs ()
  (interactive)
  (set-frame-parameter (selected-frame) 'alpha '(76 . 50)))

;; notransparency
(defun notransparency/emacs ()
  (interactive)
  (set-frame-parameter (selected-frame) 'alpha '(100 . 100)))

;; odin lsp server config

(require 'lsp-mode)

(defvar lsp-language-id-configuration '((odin-mode . "odin")))
 (lsp-register-client
 (make-lsp-client :new-connection (lsp-stdio-connection "/home/synbian/git/clone/Odin/ols/ols")
                  :major-modes '(odin-mode)
                  :server-id 'ols
                  :multi-root t)) ;; This is just so lsp-mode sends the "workspaceFolders" param to the server.
(add-hook 'odin-mode-hook #'lsp)

;;; ggtags mode enable
 (add-hook 'c-mode-common-hook
                  (lambda ()
                    (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
                      (ggtags-mode 1)
                      )))
(add-hook 'asm-mode-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
              (ggtags-mode 1)
              )))

;; turtle mode
(defun my-turtle-mode ()
  (when (and (stringp buffer-file-name)
             (string-match "\\.ttl\\'" buffer-file-name))
    (ttl-mode)))

(add-hook 'find-file-hook 'my-turtle-mode)

;;; config for lsp-lint-mode golang
;;;
(with-eval-after-load 'lsp-mode
  (lsp-register-custom-settings
   '(("golangci-lint.command"
      ["golangci-lint" "run" "--enable-all" "--disable" "lll" "--out-format" "json" "--issues-exit-code=1"])))

  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection
                                     '("golangci-lint-langserver"))
                    :activation-fn (lsp-activate-on "go")
                    :language-id "go"
                    :priority 0
                    :server-id 'golangci-lint
                    :add-on? t
                    :library-folders-fn #'lsp-go--library-default-directories
                    :initialization-options (lambda ()
                                              (gethash "golangci-lint"
                                                       (lsp-configuration-section "golangci-lint"))))))

(use-package go-mode
  :custom
  (gofmt-command "goimports")
  :hook
  (before-save-hook gofmt-before-save))

;; Elixir configuration

(after! lsp-clients
  (lsp-register-client
   (make-lsp-client :new-connection
    (lsp-stdio-connection
        (expand-file-name
          "/home/synbian/git/clone/Erlang/elixir-ls/scripts/language_server.sh"))
        :major-modes '(elixir-mode)
        :priority -1
        :server-id 'elixir-ls
        :initialized-fn (lambda (workspace)
            (with-lsp-workspace workspace
             (let ((config `(:elixirLS
                             (:mixEnv "dev"
                                     :dialyzerEnabled
                                     :json-false))))
             (lsp--set-configuration config)))))))

;; Configure LSP-ui to define when and how to display informations.
(after! lsp-ui
  (setq lsp-ui-doc-max-height 20
        lsp-ui-doc-max-width 80
        lsp-ui-sideline-ignore-duplicate t
        lsp-ui-doc-header t
        lsp-ui-doc-include-signature t
        lsp-ui-doc-position 'bottom
        lsp-ui-doc-use-webkit nil
        lsp-ui-flycheck-enable t
        lsp-ui-imenu-kind-position 'left
        lsp-ui-sideline-code-actions-prefix "💡"
        ;; fix for completing candidates not showing after “Enum.”:
        company-lsp-match-candidate-predicate #'company-lsp-match-candidate-prefix
        ))


;; Enable format and iex reload on save
(after! lsp
  (add-hook 'elixir-mode-hook
            (lambda ()
              (add-hook 'before-save-hook 'elixir-format nil t)
              (add-hook 'after-save-hook 'alchemist-iex-reload-module))))

;; Setup some keybindings for exunit and lsp-ui
(map! :mode elixir-mode
        :leader
        :desc "iMenu" :nve  "c/"    #'lsp-ui-imenu
        :desc "Run all tests"   :nve  "ctt"   #'exunit-verify-all
        :desc "Run all in umbrella"   :nve  "ctT"   #'exunit-verify-all-in-umbrella
        :desc "Re-run tests"   :nve  "ctx"   #'exunit-rerun
        :desc "Run single test"   :nve  "cts"   #'exunit-verify-single)


(defun elixir/find-mix-project (dir)
  "Try to locate a Elixir project root by 'mix.exs' above DIR."
  (let ((mix_root (locate-dominating-file dir "mix.exs")))
    (message "Found Elixir project root in '%s' starting from '%s'" mix_root dir)
    (if (stringp mix_root) `(transient . ,mix_root) nil)))

(add-hook 'project-find-functions 'elixir/find-mix-project nil nil)



;; Start by configuring Alchemist for some tasks.
(use-package! alchemist
  :hook (elixir-mode . alchemist-mode)
  :config
  (set-lookup-handlers! 'elixir-mode
    :definition #'alchemist-goto-definition-at-point
    :documentation #'alchemist-help-search-at-point)
  (set-eval-handler! 'elixir-mode #'alchemist-eval-region)
  (set-repl-handler! 'elixir-mode #'alchemist-iex-project-run)
  (setq alchemist-mix-env "dev")
  (setq alchemist-hooks-compile-on-save t)
  (map! :map elixir-mode-map :nv "m" alchemist-mode-keymap))

;; Now configure LSP mode and set the client filepath.
(use-package! lsp-mode
  :commands lsp
  :config
  (setq lsp-enable-file-watchers nil)
  :hook
  (elixir-mode . lsp))

;; Configure exunit
(use-package! exunit)

;; Enable credo checks on flycheck
;;(use-package! flycheck-credo
;;  :after flycheck
;;  :config
;;    (flycheck-credo-setup)
;;    (after! lsp-ui
;;      (flycheck-add-next-checker 'lsp-ui 'elixir-credo)))


;;; End Elixir config


(use-package vhdl-ext
  :after vhdl-mode
  :demand
  :hook ((vhdl-mode . vhdl-ext-mode))
  :init
  ;; Can also be set through `M-x RET customize-group RET vhdl-ext':
  ;;  - Vhdl Ext Feature List (provides info of different features)
  ;; Comment out/remove the ones you do not need
  (setq vhdl-ext-feature-list
        '(font-lock
          eglot
          lsp
          flycheck
          beautify
          navigation
          template
          compilation
          imenu
          which-func
          hideshow
          time-stamp
          company-keywords
          ports))
  :config
  (vhdl-ext-mode-setup))

;;; drag and drop treemacs disable
;;
(define-key treemacs-mode-map [drag-mouse-1] nil)

(defun treemacs-dragleftclick-action () nil)


;; dictionary spell checker
;; (flyspell-mode +1)

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
