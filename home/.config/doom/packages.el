;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/radian-software/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see radian-software/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)


;; disable evil-mode
(package! evil-mode :disable t)

;; disable evil-mode
(package! ccls :disable t)

(package! elfeed)

(package! v-mode)

(package! gnuplot-mode)

(package! forth-mode)

(package! vterm-toggle)

(package! cmake-mode)

(package! sonic-pi)

(package! protobuf-mode)

(package! sql-indent)

(package! origami)

(package! csv-mode)

(package! erlang)

(package! elixir-mode)

(package! csharp-mode)

(package! dotnet)

(package! dockerfile-mode)

(package! go-mode)

(package! go)

(package! org-roam)

(package! org-roam-ui)

;; (use-package! org-roam-server)

(package! lsp-ui)

(package! perspective)

;;; this single line cause the entire doom emacs to fail miserably with no good
;;; error message to support it...
;; (package! persp-mode :disable #t)
;;;; do not enable this line... any more.

(package! ggtags)

(package! ctags-update)

;;;; so mutch problems on DOOM emacs install....
;;; (package! stack-mode)

(package! yaml-mode)

(package! ocamlformat)

(package! pdf-tools)

(package! elpher)

(package! sqlite3)

(package! json-mode)

(package! nix-mode)

(package! slime)

(package! racket-mode)

(package! markdown-mode)

(package! lsp-mode)

(package! s)

(package! dash)

(package! origami)

;; (package! ccls)

(package! projectile)

(package! tide)

(package! spdx)

(package! haskell-mode)

(package! agda2-mode)

(package! vhdl-ext)

(package! godoctor)

(package! all-the-icons)

(package! orderless)

(package! flyspell)

;;;; for reason of not initialize
;; (package! xcscope)

;; (package! clang-format)

;; (package! neotree)

;; (package! uiua-mode)

;; (package! reazon)
;;;;

;; (package! json-reformat)

;;; not working well . | not tested well
;; (package! vimish-fold)

;; (package! hs-minor-mode) ;; buildin

;; (package! neotree)



  ;;  nov projectile emms-soundcloud emms  vterm-toggle vterm ggtags gemini-write cmake-mode lsp-ui treeacs-tab-bar treemacs-perspective treemacs-all-the-icons treemacs
  ;;  java-snippets java-imports elisp-sandbox origami omnisharp hippie-expand-slime geiser-mit php-mode
  ;;  fill-column-indicator keycast company merlinq   keyfreq  org-ac helm-org-recent-headings mqtt-mode
  ;;  qt-pro-mode treemacs-icons-dired snippet lsp-treemacs treemacs-projectile lsp-java lsp-haskell scala-mode
  ;;  clojure-mode epm multi-web-mode flymake-eslint helpful flymake-jslint flymake-json js2-mode lsp-mode
  ;;  auto-package-update tern-auto-complete tern magit ranger dired-subtree  nixos-options haskell-mode
  ;;  hindent org org-mode exwm lisp-mode scheme-complete lispy  rcirc-menu circe hy-mode dr-racket-like-unicode
  ;;  zoom-window quack racer lsp-rust cargo flycheck-rust use-package emmet-mode
  ;;   noflet code-archive js-auto-beautify
  ;; vue-mode color-theme-modern async web web-beautify)
